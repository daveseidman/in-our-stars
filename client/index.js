import './index.scss';
import { createEl, addEl } from 'lmnt';
import onChange from 'on-change';
import SignaturePad from 'signature_pad';
import autoBind from 'auto-bind';

class App {
  constructor() {
    autoBind(this);

    const state = {

    };
    this.el = createEl('div', { className: 'app' });

    this.canvas = createEl('canvas', { className: 'canvas' });
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.signaturePad = new SignaturePad(this.canvas, { backgroundColor: 'black', penColor: 'white' });

    this.undoButton = createEl('button', { className: 'undo', innerText: 'undo' }, {}, { click: this.undo });

    this.selector = createEl('radio', { className: 'selector', name: 'drawing type', values: ['free draw', 'text', 'upload / take photo'] });


    addEl(this.el, this.canvas, this.undoButton, this.selector);
    addEl(this.el);

    this.state = onChange(state, this.update); // monitor state changes
  }

  update(change, current, previous) {
    if (window.location.origin.indexOf('localhost') >= 0) console.log(`state changed: ${change}: ${previous} -> ${current}`);
  }

  undo() {
    const data = this.signaturePad.toData();
    if (data) {
      data.pop();
      this.signaturePad.fromData(data);
    }
  }

  resize() {

  }
}

const app = new App();
if (window.location.origin.indexOf('localhost') >= 0) window.app = app;
